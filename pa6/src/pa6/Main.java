package pa6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;

import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * Project name: Cocos 2D HTML
 * @author Parinvut Rochanavedya
 * @version 26-04-2015
 */
public class Main extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	public JTextField time;
	public JTextField distance;
	public JTextField velocity;
	public JTextField acceleration;
	private JButton btnRun, btnPause, btnReset;
	
	public JFrame me = this;
	
	public double t = 0, s = 0, v = 0, a = 0;
	public Thread th;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
					
					TimeRunner.init();
					TimeRunner timer = TimeRunner.getInstance();
					timer.addGUI(frame);
					
					frame.th = new Thread(timer);
					
					frame.time.setText(String.format("%.5f", frame.t));
					frame.distance.setText(String.format("%.5f", frame.s));
					frame.velocity.setText(String.format("%.5f", frame.v));
					frame.acceleration.setText(String.format("%.5f", frame.a));
					
					frame.btnRun.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0){
							if(!timer.isRunning()){
								timer.pauseOrResume();
							}
							frame.th.start();
							
						}
					});
					
					frame.btnPause.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							if(timer.isRunning()){
								timer.pauseOrResume();
							}
							
						}
					});
					
					frame.btnReset.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							if(timer.isRunning()){
								timer.pauseOrResume();
							}
							//frame.btnRun.setEnabled(true);
							//frame.btnPause.setEnabled(false);
							timer.time = 0;
							frame.t = 0;
							frame.s = 0;
							frame.v = 0;
							frame.a = 0;
							frame.time.setText(String.format("%.5f", frame.t));
							frame.distance.setText(String.format("%.5f", frame.s));
							frame.velocity.setText(String.format("%.5f", frame.v));
							frame.acceleration.setText(String.format("%.5f", frame.a));
							frame.th.stop();
						}
					});
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setResizable(false);
		setTitle("Cocos 2D html");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 445, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		btnRun = new JButton("Run");
		
		JLabel lblTime = new JLabel("Time :");
		
		JLabel lblDistance = new JLabel("Distance :");
		
		JLabel lblVelocity = new JLabel("Velocity :");
		
		btnReset = new JButton("Reset");
		
		
		JLabel lblAcceleration = new JLabel("Acceleration : ");
		
		time = new JTextField();
		time.setHorizontalAlignment(SwingConstants.RIGHT);
		time.setText("0");
		time.setEditable(false);
		time.setColumns(10);
		
		distance = new JTextField();
		distance.setHorizontalAlignment(SwingConstants.RIGHT);
		distance.setText("0");
		distance.setEditable(false);
		distance.setColumns(10);
		
		velocity = new JTextField();
		velocity.setHorizontalAlignment(SwingConstants.RIGHT);
		velocity.setText("0");
		velocity.setColumns(10);
		
		acceleration = new JTextField();
		acceleration.setHorizontalAlignment(SwingConstants.RIGHT);
		acceleration.setText("0");
		acceleration.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		
		JComboBox comboBox_1 = new JComboBox();
		
		JComboBox comboBox_2 = new JComboBox();
		
		JComboBox comboBox_3 = new JComboBox();
		
		btnPause = new JButton("Pause");
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(26)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
							.addComponent(lblDistance, GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)
							.addComponent(lblTime, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(lblVelocity, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addComponent(lblAcceleration))
					.addGap(45)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(acceleration, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(comboBox_3, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(velocity, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(comboBox_2, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(distance, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(time, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(75, Short.MAX_VALUE))
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnRun, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnPause, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
					.addComponent(btnReset, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
					.addGap(18))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(22)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTime)
						.addComponent(time, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(27)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDistance)
						.addComponent(distance, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(33)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblVelocity)
						.addComponent(velocity, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboBox_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(30)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAcceleration)
						.addComponent(acceleration, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboBox_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnRun)
						.addComponent(btnReset)
						.addComponent(btnPause))
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}
}
