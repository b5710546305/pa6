package pa6;

import java.util.Timer;
import java.util.TimerTask;

/**
 * The time increaser object (singleton)
 * @author Parinvut Rochanavedya
 */
public class TimeRunner implements Runnable{
	/**Singleton attribute*/
	private static TimeRunner me;
	/**
	 * Times
	 */
	public int time;
	public final static long INTERVAL = 1000; // millisec
	private Main gui;
	private Timer timer;
	private TimerTask task;
	private boolean running = false;
	
	
	/**
	 * Constructor
	 */
	public TimeRunner(){
		if(me == null){
			me = this;
			timer = new Timer();
			task = new TimerTask(){
				@Override
				public void run() {
					if(running){
						time++;
					}
					gui.t = time;
					try{
						gui.a = Double.parseDouble(gui.acceleration.getText());
						gui.v = Double.parseDouble(gui.velocity.getText());
					} catch (NumberFormatException e){
						gui.a = 0;
					}
					if(running){
						gui.v += gui.a;
						gui.s += gui.v;
						gui.time.setText(String.format("%.5f", gui.t));
						gui.distance.setText(String.format("%.5f", gui.s));
						gui.velocity.setText(String.format("%.5f", gui.v));
						gui.acceleration.setText(String.format("%.5f", gui.a));
					}
					
					
				}
			};
		}
		time = 0;
	}
	
	@Override
	public void run(){
		long delay = 1000 - System.currentTimeMillis()%1000; //changes every time run() is called
		timer.scheduleAtFixedRate( task,  delay, INTERVAL );
	}
	
	/**
	 * Make the Singleton of this class
	 */
	public static void init(){
		me = new TimeRunner();
	}
	
	/**
	 * Get the instance
	 * @return this thing
	 */
	public static TimeRunner getInstance(){
		return me;
	}
	
	/**
	 * set time
	 */
	public void setTime(int t){
		time = t;
	}
	
	/**
	 * Pause or Resume Timer
	 */
	public void pauseOrResume(){
		if(running){
			running = false;
		}else {
			running = true;
		}
	}
	
	/**
	 * Check if it's running
	 */
	public boolean isRunning(){
		if(running){
			return true;
		}
		return false;
	}
	
	/**
	 * add prefered gui
	 */
	public void addGUI(Main gui){
		this.gui = gui;
	}
}